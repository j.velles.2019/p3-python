import unittest
import math  # Importa el módulo math
from mysoundsin import SoundSin


class TestSoundSin(unittest.TestCase):

    def test_creation(self):
        # Prueba la creación de un objeto SoundSin
        sound_sin = SoundSin(1.0, 440, 10000)
        self.assertIsInstance(sound_sin, SoundSin)

    def test_duration_inherited(self):
        # Prueba que la duración se inicializa correctamente
        # en un objeto SoundSin
        sound_sin = SoundSin(2.0, 440, 10000)
        self.assertEqual(sound_sin.duration, 2.0)

    def test_sin_generation(self):
        # Prueba que la señal sinusoidal se genera correctamente
        # en un objeto SoundSin
        sound_sin = SoundSin(1.0, 440, 10000)
        # Comprueba las primeras muestras (primeras 10)
        expected_samples = [
            int(10000 * math.sin(
                2 * math.pi * 440 * t / sound_sin.samples_second
            )) for t in range(10)
        ]
        for i in range(10):
            self.assertEqual(sound_sin.buffer[i], expected_samples[i])


if __name__ == '__main__':
    unittest.main()
