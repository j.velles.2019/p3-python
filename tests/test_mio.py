import unittest
import mysound
import math


class TestSound(unittest.TestCase):

    def test_duration_initialization(self):
        # Crear una instancia de Sound con duración 2 segundos
        sound = mysound.Sound(2.0)
        # Verificar que la duración se ha inicializado correctamente
        self.assertEqual(sound.duration, 2.0)

    def test_sin_wave_generation(self):
        # Crear una instancia de Sound con duración de 1 segundo
        sound = mysound.Sound(1.0)
        # Generar una onda sinusoidal de 440 Hz y amplitud 10000
        sound.sin(440, 10000)

        # Comprobar las primeras muestras (primeras 10)
        expected_samples = [
            int(10000 * math.sin(
                2 * math.pi * 440 * t / sound.samples_second
            )) for t in range(10)
        ]

        for i in range(10):
            self.assertEqual(sound.buffer[i], expected_samples[i])


if __name__ == '__main__':
    unittest.main()
