import unittest
from mysound import Sound
from soundops import soundadd


class TestSoundAdd(unittest.TestCase):

    def test_add_equal_length(self):
        # Prueba la suma de dos sonidos de igual longitud
        sound1 = Sound(1.0)
        sound1.buffer = [1, 2, 3]

        sound2 = Sound(1.0)
        sound2.buffer = [4, 5, 6]

        result = soundadd(sound1, sound2)
        self.assertEqual(result.buffer, [5, 7, 9])

    def test_add_different_length(self):
        # Prueba la suma de dos sonidos de diferentes longitudes
        sound1 = Sound(1.0)
        sound1.buffer = [1, 2, 3]

        sound2 = Sound(0.5)
        sound2.buffer = [4, 5]

        result = soundadd(sound1, sound2)
        self.assertEqual(result.buffer, [5, 7, 3])

    def test_add_no_empty_sound(self):
        # Prueba la suma de un sonido con otro sonido vacio
        sound1 = Sound(1.0)
        sound1.buffer = [1, 2, 3]

        sound2 = Sound(1.0)
        sound2.buffer = None

        result = soundadd(sound1, sound2)
        self.assertEqual(result.buffer, [1, 2, 3])


if __name__ == '__main__':
    unittest.main()
