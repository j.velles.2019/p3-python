from mysound import Sound


def soundadd(s1: Sound, s2: Sound) -> Sound:
    """
    Suma dos sonidos (objetos de la jerarquía Sound).

    :param s1: Primer sonido a sumar
    :param s2: Segundo sonido a sumar
    :return: Sonido resultante de la suma
    """
    # Calcula la longitud del sonido resultante
    # como la longitud del sonido no vacío
    if not s1.buffer:
        return s2
    elif not s2.buffer:
        return s1

    max_length = max(len(s1.buffer), len(s2.buffer))

    # Inicializa un nuevo sonido con la longitud calculada
    result_sound = Sound(max_length / s1.samples_second)

    # Suma las muestras de s1 y s2 hasta que uno de ellos se agote
    for i in range(max_length):
        if i < len(s1.buffer) and i < len(s2.buffer):
            result_sound.buffer[i] = s1.buffer[i] + s2.buffer[i]
        elif i < len(s1.buffer):
            result_sound.buffer[i] = s1.buffer[i]
        else:
            result_sound.buffer[i] = s2.buffer[i]

    return result_sound
